## Configurações

Esse projeto utiliza o CocoaPods como gerenciador de dependência então para executar o projeto **primeiro** deve ter o <a href="http://www.quora.com/Adam-DAngelo">CocoaPods</a> intslado.

Caso já tenha instaldo abra o terminal e navegue até o diretório do projeto e execute o seguinte comando:
```
pod install
```

**Para abrir o projeto deve ser utilizado o arquivo IBM FC.xcworkspace que é gerado após a execução do comando acima.**
