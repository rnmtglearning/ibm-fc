//
//  ResultsInteractor.swift
//  IBM FC
//
//  Created by user140115 on 5/6/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import RealmSwift

class ResultsInteractor: ResultsInteractorModule {
    
    func loadStrikers(response: @escaping ([Striker]?) -> Void) {
        RequestAPI.shared.json("/strikers") { (data, code, status) in
            if let data = data {
                do {
                    let json = try JSONDecoder().decode(StrikerResponse.self, from: data)
                    response(json.data)
                } catch {
                }
            }
        }
    }
    
    func loadMatches(response: @escaping ([Match]) -> Void) {
        var matches = [Match]()
        
        RequestAPI.shared.json("/games") { (data, code, status) in
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    if let matchesResponse = dictionary["data"] as? [[String : Any]] {
                        for obj in matchesResponse {
                            let match = Match()
                            match.gameId = obj["gameId"] as? Int ?? 0
                            match.teamAwayId = obj["teamAwayId"] as? Int ?? 0
                            match.teamHomeId = obj["teamHomeId"] as? Int ?? 0
                            match.scoreHomeId = obj["scoreHomeId"] as? Int ?? 0
                            match.scoreAwayId = obj["scoreAwayId"] as? Int ?? 0
                            matches.append(match)
                        }
                    }
                }
            }
            
            let realm = try! Realm()
            let results = realm.objects(Match.self)
            results.forEach { (match) in
                matches.append(match)
            }
            
            response(matches)
        }
    }
    
}
