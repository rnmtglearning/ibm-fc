//
//  ResultsRouter.swift
//  IBM FC
//
//  Created by user140115 on 5/4/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class ResultsRouter: ResultsRouterModule {
    
    fileprivate var window: UIWindow?
    fileprivate var nav: UINavigationController?
    
    func navigateResults(window: UIWindow?) {
        var presenter = ResultsConfig.shared.configure()
        presenter.router = self
        
        if let view = presenter.view as? ResultsViewController {
            let nav = UINavigationController(rootViewController: view)

            window?.rootViewController = nav
            window?.makeKeyAndVisible()
            self.window = window
            self.nav = nav
        }
    }
    
    func navigateLogin() {
        if let window = self.window {
            let router = LoginRouter()
            router.navigateLogin(window: window)
        }
    }
    
    func navigateNewMatch() {
        if let nav = self.nav {
            let router = NewMatchRouter()
            router.navigateNewMatch(from: nav)
        }
    }
}
