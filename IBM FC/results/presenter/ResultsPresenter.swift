//
//  ResultsPresenter.swift
//  IBM FC
//
//  Created by user140115 on 5/4/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import RealmSwift

class ResultsPresenter: ResultsPresenterModule {
    
    var view: ResultsViewModule?
    var router: ResultsRouterModule?
    var interactor: ResultsInteractorModule?
    
    func close() {
        KeychainWrapper.remove(key: .token)
        UserDefaults.setBool(value: false, key: .isLogged)
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(Match.self))
        }
        router?.navigateLogin()
    }
    
    func navigateNewMatch() {
        router?.navigateNewMatch()
    }
    
    func loadStrikers() {
        interactor?.loadStrikers(response: { (strikers) in
            if let strikers = strikers {
                self.view?.showStrikers(strikers.sorted(by: { (first, second) -> Bool in
                    return first.goals ?? 0 > second.goals ?? 0
                }))
            }
        })
    }
    
    func loadMatches() {
        interactor?.loadMatches(response: { (matches) in
            var teams = [Int: Team]()
            matches.forEach({ (match) in
                var teamHome = teams[match.teamHomeId]
                if teamHome == nil {
                    teamHome = Team()
                    teamHome?.id = match.teamHomeId
                }
                
                print(match.teamHomeId)
                print(match.teamAwayId)

                var teamAway = teams[match.teamAwayId]
                if teamAway == nil {
                    teamAway = Team()
                    teamAway?.id = match.teamAwayId
                }
                
                teamHome?.setGoals(favor: match.scoreHomeId, against: match.scoreAwayId)
                teamAway?.setGoals(favor: match.scoreAwayId, against: match.scoreHomeId)
                
                let countryHome = Countries.values.filter({ (key, _) -> Bool in key == teamHome?.id ?? 0 }).first
                let countryAway = Countries.values.filter({ (key, _) -> Bool in key == teamAway?.id ?? 0 }).first
                
                teamHome?.name = countryHome?.1
                teamAway?.name = countryAway?.1
                teamHome?.flag = UIImage(named: "\(countryHome?.0 ?? 0)-\((countryHome?.1 ?? "").replacingOccurrences(of: " ", with: "_"))")
                teamAway?.flag = UIImage(named: "\(countryAway?.0 ?? 0)-\((countryAway?.1 ?? "").replacingOccurrences(of: " ", with: "_"))")

                if let home = teamHome, let away = teamAway {
                    teams.updateValue(home, forKey: home.id ?? 0)
                    teams.updateValue(away, forKey: away.id ?? 0)
                }
            })
                        
            self.view?.showResults(teams.sorted(by: { (team1, team2) -> Bool in
                return team1.value.points > team2.value.points
            }))
        })
    }
}
