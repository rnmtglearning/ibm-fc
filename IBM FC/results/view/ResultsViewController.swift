//
//  ResultsViewController.swift
//  IBM FC
//
//  Created by user140115 on 5/4/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController, ResultsViewModule {
    
    var presenter: ResultsPresenterModule?
    
    var strikersView: StrikerCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        return StrikerCollectionView(frame: .zero, collectionViewLayout: layout)
    }()
    
    var resultsView: ResultsCollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        return ResultsCollectionView(frame: .zero, collectionViewLayout: layout)
    }()
    
    var strikerHeader: UILabel = {
        let l = UILabel()
        l.text = "ARTILHEIROS"
        l.font = UIFont.boldSystemFont(ofSize: 14)
        l.textColor = UIColor.white
        l.textAlignment = .center
        l.backgroundColor = UIColor.royalBlue
        return l
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.whiteSmoke
        
        setupNavigationController()
        setupView()
        presenter?.loadStrikers()
        presenter?.loadMatches()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.loadMatches()
    }
    
    fileprivate func setupView() {
        self.view.addSubview(strikersView)
        self.view.addSubview(strikerHeader)
        self.view.addSubview(resultsView)
        self.view.addConstraints(pattern: "H:|[v0]|", views: strikersView, strikerHeader, resultsView)
        self.view.addConstraint(pattern: "V:|[v0]-0-[v1(18)]-10-[v2(100)]-10-|", views: resultsView, strikerHeader, strikersView)
    }
    
    fileprivate func setupNavigationController() {
        self.title = "Tabela"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sair", style: .plain, target: self, action: #selector(self.closeAction))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_add"), style: .plain, target: self, action: #selector(self.newMatchAction))
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Tabela", style: .plain, target: self, action: nil)
    }
    
    @objc fileprivate func closeAction() {
        alert(title: "Deseja realmente sair?", message: "Ao sair todos os jogos adicionados serão perdidos.", button: "Sair", buttonCancel: "Cancelar") { (action) in
            self.presenter?.close()
        }
    }
    
    @objc fileprivate func newMatchAction() {
        self.presenter?.navigateNewMatch()
    }
    
    func showStrikers(_ strikers: [Striker]) {
        strikersView.datasource = strikers
        strikersView.reloadData()
    }
    
    func showResults(_ results: [(Int, Team)]) {
        resultsView.datasource = results
        resultsView.reloadData()
    }

}
