//
//  ResultsCollectionView.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 07/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import UIKit

class ResultsCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var datasource: [(Int, Team)]?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        self.delegate = self
        self.dataSource = self
        self.backgroundColor = UIColor.lightGray
        
        if let layout = layout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 5
        }
        
        self.register(ResultsHeaderViewCell.self, forCellWithReuseIdentifier: "reuse")
        self.register(ResultsHeaderViewCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if let cell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as? ResultsHeaderViewCell {
            cell.points = cell.configureLabel(cell.points, header: true)
            cell.victory = cell.configureLabel(cell.victory, header: true)
            cell.draw = cell.configureLabel(cell.draw, header: true)
            cell.loss = cell.configureLabel(cell.loss, header: true)
            cell.balance = cell.configureLabel(cell.balance, header: true)
            
            cell.points.text = "P"
            cell.victory.text = "V"
            cell.draw.text = "E"
            cell.loss.text = "D"
            cell.balance.text = "SG"
            cell.team.text = "TIMES"
            
            cell.backgroundColor = UIColor.lightGray
            
            return cell
        }
        
        return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath) as? ResultsHeaderViewCell {
            if let team = datasource?[indexPath.row].1{
                cell.flag.image = team.flag
                cell.team.text = team.name
                cell.victory.text = team.victory.description
                cell.draw.text = team.draw.description
                cell.loss.text = team.loss.description
                cell.balance.text = team.balance.description
                cell.points.text = team.points.description
            }
            return cell
        }
        return collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.width, height: 50)
    }
    
}
