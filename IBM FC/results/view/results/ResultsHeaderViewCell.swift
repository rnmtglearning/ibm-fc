//
//  ResultsHeaderViewCell.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 07/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class ResultsHeaderViewCell: UICollectionViewCell {
    
    lazy var points = self.configureLabel()
    lazy var victory = self.configureLabel()
    lazy var draw = self.configureLabel()
    lazy var loss = self.configureLabel()
    lazy var balance = self.configureLabel()
    
    lazy var fields: UIStackView = {
        let s = UIStackView()
        s.axis = .horizontal
        s.distribution = .fillEqually
        s.alignment = .center
        s.spacing = 3
        
        s.addArrangedSubview(points)
        s.addArrangedSubview(victory)
        s.addArrangedSubview(draw)
        s.addArrangedSubview(loss)
        s.addArrangedSubview(balance)
        
        s.addConstraints(pattern: "V:|[v0]|", views: points, victory, draw, loss, balance)
        return s
    }()
    
    var team: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.oxfordBlue
        l.font = UIFont.systemFont(ofSize: 20)
        l.textAlignment = .center
        l.text = "S"
        return l
    }()
    
    var flag = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        
        self.addSubview(flag)
        self.addSubview(team)
        self.addSubview(fields)
        self.addConstraint(pattern: "H:|-10-[v0(40)]-5-[v1]-5-[v2(\(self.frame.width * 0.4))]-5-|", views: flag, team, fields)
        self.addConstraints(pattern: "V:|[v0]|", views: fields, team)
        self.addConstraint(pattern: "V:|-10-[v0]-10-|", views: flag)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureLabel(_ l: UILabel = UILabel(), header: Bool = false) -> UILabel {
        l.textColor = UIColor.oxfordBlue
        l.font = header ? UIFont.boldSystemFont(ofSize: 20) : UIFont.systemFont(ofSize: 20)
        l.textAlignment = .center
        l.backgroundColor = header ? UIColor.oxfordBlueLight : UIColor.prussianBlueLight
        l.text = "S"
        return l
    }
}
