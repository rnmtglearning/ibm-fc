//
//  StrikerViewCell.swift
//  IBM FC
//
//  Created by user140115 on 5/6/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class StrikerViewCell: UICollectionViewCell {
    
    let player: UIImageView = {
        let i = UIImageView()
        i.contentMode = .scaleToFill
        i.backgroundColor = UIColor.darkGray
        return i
    }()
    
    let flag: UIImageView = {
        let i = UIImageView()
        i.contentMode = .scaleToFill
        i.image = #imageLiteral(resourceName: "9-Argentina")
        return i
    }()
    
    let score: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 12)
        l.text = "8 Gols"
        l.textColor = .white
        l.textAlignment = .center
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.oxfordBlue
        
        self.addSubview(player)
        self.addSubview(flag)
        self.addSubview(score)
        
        self.addConstraint(pattern: "H:|[v0]|", views: player)
        self.addConstraint(pattern: "H:|-5-[v0(25)]-2-[v1]-5-|", views: flag, score)
        self.addConstraint(pattern: "V:|[v0(70)]-8-[v1]-8-|", views: player, flag)
        
        self.addConstraint(NSLayoutConstraint(item: score, attribute: .centerY, relatedBy: .equal, toItem: flag, attribute: .centerY, multiplier: 1, constant: 0))
        
        self.layer.radius(5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
