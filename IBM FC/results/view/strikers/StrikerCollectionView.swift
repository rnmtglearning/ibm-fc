//
//  StrikerCollectionView.swift
//  IBM FC
//
//  Created by user140115 on 5/6/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class StrikerCollectionView: UICollectionView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    var datasource: [Striker]?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        delegate = self
        dataSource = self
        backgroundColor = UIColor.whiteSmoke
        self.register(StrikerViewCell.self, forCellWithReuseIdentifier: "reuse")
        
        if let layout = layout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 15
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath) as? StrikerViewCell {
            let striker = datasource?[indexPath.row]
            let country = Countries.values.filter { $0.0 == striker?.teamId ?? 0}.first

            cell.player.dowloadImage(url: striker?.playerPhotoURL ?? "")
            cell.flag.image = UIImage(named: "\(String(describing: country?.0 ?? 0))-\(String(describing: country?.1.replacingOccurrences(of: " ", with: "_") ?? ""))")
            cell.score.text = "\(striker?.goals ?? 0) goals"
            return cell
        }
        return collectionView.dequeueReusableCell(withReuseIdentifier: "reuse", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: self.frame.height)
    }
    
}
