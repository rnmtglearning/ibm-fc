//
//  ResultsConfig.swift
//  IBM FC
//
//  Created by user140115 on 5/4/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

protocol ResultsInteractorModule {
    
    func loadStrikers(response: @escaping ([Striker]?) -> Void)
    
    func loadMatches(response: @escaping ([Match]) -> Void)

}

protocol ResultsRouterModule {
    
    func navigateResults(window: UIWindow?)
    
    func navigateLogin()
    
    func navigateNewMatch()
}

protocol ResultsViewModule {
    var presenter: ResultsPresenterModule? { get set }
    
    func showStrikers(_ strikers: [Striker])
    
    func showResults(_ results: [(Int, Team)])
}

protocol ResultsPresenterModule {
    var view: ResultsViewModule? { get set }
    var router: ResultsRouterModule? { get set }
    var interactor: ResultsInteractorModule? { get set }
    
    func close()
    
    func navigateNewMatch()
    
    func loadStrikers()
    
    func loadMatches() 
}

class ResultsConfig: Configurable {
    
    typealias T = ResultsPresenterModule
    
    static let shared = ResultsConfig()
    
    func configure() -> ResultsPresenterModule {
        
        let view = ResultsViewController()
        let presenter = ResultsPresenter()
        let interactor = ResultsInteractor()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        
        return presenter
    }
    
}
