//
//  Striker.swift
//  IBM FC
//
//  Created by user140115 on 5/6/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

struct StrikerResponse {
    var status: String?
    var data: [Striker]?
}

extension StrikerResponse: Decodable {
    enum Key: String, CodingKey {
        case status
        case data
    }
    
    init(from decoder: Decoder) throws {
        let json = Json(decoder: decoder, keyedBy: StrikerResponse.Key.self)
        
        self.init(status: json.string(forKey: .status), data: json.array(forKey: .data))
    }
}

struct Striker {
    var playerId: Int?
    var playerName: String?
    var goals: Int?
    var teamId: Int?
    var playerPhotoURL: String?
}

extension Striker: Decodable {
    enum Key: String, CodingKey {
        case playerId
        case playerName
        case goals
        case teamId
        case playerPhotoURL
    }
    
    init(from decoder: Decoder) throws {
        let json = Json(decoder: decoder, keyedBy: Striker.Key.self)
        
        self.init(playerId: json.int(forKey: .playerId),
                  playerName: json.string(forKey: .playerName),
                  goals: json.int(forKey: .goals),
                  teamId: json.int(forKey: .teamId),
                  playerPhotoURL: json.string(forKey: .playerPhotoURL))
    }
}
