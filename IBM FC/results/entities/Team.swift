//
//  Team.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 07/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class Team {
    
    var id: Int?
    var flag: UIImage?
    var name: String?
    var favorGoals: Int = 0
    var againstGoals: Int = 0
    var victory: Int = 0
    var draw: Int = 0
    var loss: Int = 0
    var points: Int {
        get {
            return victory  * 3 + draw
        }
    }
    var balance: Int {
        get {
            return favorGoals - againstGoals
        }
    }
    
    func setGoals(favor: Int, against: Int) {
        self.favorGoals += favor
        self.againstGoals += against
        
        if favor > against {
            self.victory += 1
        } else if against > favor {
            self.loss += 1
        } else {
            self.draw += 1
        }
    }
}
