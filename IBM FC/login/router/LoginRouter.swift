//
//  LoginRouter.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class LoginRouter: LoginRouterModule {
    
    fileprivate var window: UIWindow?
    
    func navigateLogin(window: UIWindow?) {
        var presenter = LoginConfig.shared.configure()
        presenter.router = self
        
        if let view = presenter.view as? LoginViewController {
            window?.rootViewController = view
            window?.makeKeyAndVisible()
            self.window = window
        }
    }
    
    func navigateResults() {
        if let window = self.window {
            let router = ResultsRouter()
            router.navigateResults(window: window)
        }
    }
    
}
