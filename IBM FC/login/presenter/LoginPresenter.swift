//
//  LoginPresenter.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class LoginPresenter: LoginPresenterModule {
    var view: LoginViewModule?
    var router: LoginRouterModule?
    var interactor: LoginInteractorModule?
    
    func login(email: String, password: String) {
        if (email.isValidEmail() && password.count >= 3) {
            interactor?.login(email: email, password: password, response: { (status, token) in
                if let token = token, status {
                    let saved = KeychainWrapper.setString(value: token, key: .token)
                    print(KeychainWrapper.loadString(key: .token) as Any)
                    if (saved) {
                        UserDefaults.setBool(value: true, key: .isLogged)
                        self.router?.navigateResults()
                    } else {
                        self.view?.showMessage("Falha ao autenticar usuário.")
                    }
                } else {
                    self.view?.showMessage("Usuário inválido.")
                }
            })
        } else {
            view?.showMessage("Email ou senha inválido.")
        }
    }

}
