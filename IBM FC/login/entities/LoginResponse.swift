//
//  LoginResponse.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

struct LoginResponse {
    var success: Bool?
    var data: LoginResponseData?
}

extension LoginResponse: Decodable {
    enum Key: String, CodingKey {
        case success
        case data
    }
    
    init(from decoder: Decoder) throws {
        let json = Json(decoder: decoder, keyedBy: LoginResponse.Key.self)
        
        self.init(success: json.bool(forKey: .success), data: try! json.container.decodeIfPresent(LoginResponseData.self, forKey: .data))
    }
}

struct LoginResponseData {
    var email: String?
    var token: String?
}

extension LoginResponseData: Decodable {
    enum Key: String, CodingKey {
        case email
        case token
    }
    
    init(from decoder: Decoder) throws {
        let json = Json(decoder: decoder, keyedBy: LoginResponseData.Key.self)
        
        self.init(email: json.string(forKey: .email), token: json.string(forKey: .token))
    }
}

//"success": true,
//"data": {
//    "email": "carlosalberto@gmail.com",
//    "token": "yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw1a1"
//}

