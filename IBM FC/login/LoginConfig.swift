//
//  LoginConfig.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

protocol LoginInteractorModule {
    func login(email: String?, password: String?, response: @escaping (Bool, String?) -> Void)
}

protocol LoginPresenterModule {
    var view: LoginViewModule? { get set }
    var router: LoginRouterModule? { get set }
    var interactor: LoginInteractorModule? { get set }
    
    func login(email: String, password: String)
    
}

protocol LoginRouterModule {
    
    func navigateLogin(window: UIWindow?)
    
    func navigateResults()
}

protocol LoginViewModule {
    var presenter: LoginPresenterModule? { get set }
    
    func showMessage(_ message: String)
}

class LoginConfig: Configurable {
    
    typealias T = LoginPresenterModule
    
    static let shared = LoginConfig()
    
    func configure() -> LoginPresenterModule {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        
        let view = LoginViewController()
        let presenter = LoginPresenter()
        let interactor = LoginInteractor()
        
        view.presenter = presenter
        presenter.view = view
        presenter.interactor = interactor
        
        return presenter
    }
    
}
