//
//  LoginInteractor.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

class LoginInteractor: LoginInteractorModule {
    
    func login(email: String?, password: String?, response: @escaping (Bool, String?) -> Void) {
        let parameters: [String : Any] = [
            "username": email ?? "",
            "password": password ?? ""
        ]
        
        RequestAPI.shared.json("/login", method: .post, parameters: parameters, callback: { (data, code, status) in
            if let data = data {
                do {
                    let json = try JSONDecoder().decode(LoginResponse.self, from: data)
                    response(status, json.data?.token ?? "")
                } catch {
                    response(false, nil)
                }
            }
        })
    }
    
}
