//
//  MovingField.swift
//  Moving
//
//  Created by Ramon Cardoso on 16/02/18.
//  Copyright © 2018 AppMedia LTDA. All rights reserved.
//

import Foundation
import UIKit

class IBMTextField: UIView, UITextFieldDelegate {
    
    let background: UIView = {
        let b = UIView()
        b.backgroundColor = .white
        b.layer.radius(22.5, color: UIColor.darkGray)
        return b
    }()
    
    lazy var field: UITextField = {
        let field = UITextField()
        field.placeholder = nil
        field.returnKeyType = .done
        field.delegate = self
        field.font = UIFont.systemFont(ofSize: 18)
        field.clearButtonMode = .whileEditing
        return field
    }()
    
    var image = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(background)
        addConstraint(pattern: "V:|[v0]|", views: background)
        addConstraint(pattern: "H:|[v0]|", views: background)
        
        background.addSubview(field)
        background.addSubview(image)
        
        background.addConstraint(pattern: "H:|-15-[v0(15)]-15-[v1]-5-|", views: image, field)
        background.addConstraint(pattern: "V:|[v0]|", views: field)
        background.addConstraint(pattern: "V:[v0(15)]", views: image)
        background.addConstraint(NSLayoutConstraint(item: image, attribute: .centerY, relatedBy: .equal, toItem: background, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
