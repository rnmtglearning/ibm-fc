//
//  LoginViewController.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, LoginViewModule {
    
    var presenter: LoginPresenterModule?
    
    let backgroundCard: UIView = {
        let v = UIView()
        v.backgroundColor = .whiteSmoke
        v.alpha = 0
        v.layer.radius(5)
        return v
    }()
    
    let logoImage : UIImageView = {
        let i = UIImageView(image: #imageLiteral(resourceName: "ic_soccer_ball"))
        i.translatesAutoresizingMaskIntoConstraints = false
        return i
    }()
    
    let email: IBMTextField = {
        let t = IBMTextField()
        t.image.image = #imageLiteral(resourceName: "ic_person")
        t.field.keyboardType = .emailAddress
        t.field.autocapitalizationType = .none
        t.field.placeholder = "Login"
        return t
    }()
    
    let password: IBMTextField = {
        let t = IBMTextField()
        t.image.image = #imageLiteral(resourceName: "ic_password")
        t.field.isSecureTextEntry = true
        t.field.placeholder = "Senha"
        return t
    }()
    
    lazy var login: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor.prussianBlue
        b.layer.radius(22.5)
        b.setTitle("Entrar", for: .normal)
        b.addTarget(self, action: #selector(self.loginAction), for: .touchUpInside)
        return b
    }()
    
    
    lazy var fields: UIStackView = {
        let s = UIStackView()
        s.axis = .vertical
        s.distribution = .fillEqually
        s.alignment = .center
        s.spacing = 10
        
        s.addArrangedSubview(email)
        s.addArrangedSubview(password)
        s.addArrangedSubview(login)
        
        s.addConstraints(pattern: "H:|[v0]|", views: email, password, login)
        s.addConstraint(pattern: "V:[v0(45)]", views: email, password, login)
        
        s.setCustomSpacing(40, after: password)
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        backgroundCard.fadeIn()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    fileprivate func setupView() {
        self.view.backgroundColor = .oxfordBlue
        
        self.view.addSubview(backgroundCard)
        self.view.addSubview(logoImage)
        
        self.view.addConstraint(pattern: "H:|-20-[v0]-20-|", views: backgroundCard)
        self.view.addConstraint(pattern: "V:[v0(300)]", views: backgroundCard)
        
        self.view.addConstraint(NSLayoutConstraint(item: backgroundCard, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 30))
        self.view.addConstraint(NSLayoutConstraint(item: backgroundCard, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
        
        self.view.addConstraint(NSLayoutConstraint(item: logoImage, attribute: .centerX, relatedBy: .equal, toItem: backgroundCard, attribute: .centerX, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: logoImage, attribute: .bottom, relatedBy: .equal, toItem: backgroundCard, attribute: .top, multiplier: 1, constant: logoImage.frame.height / 5))
        
        backgroundCard.addSubview(fields)
        
        backgroundCard.addConstraint(pattern: "H:|-10-[v0]-10-|", views: fields)
        backgroundCard.addConstraint(NSLayoutConstraint(item: fields, attribute: .centerY, relatedBy: .equal, toItem: backgroundCard, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    @objc func loginAction() {
        presenter?.login(email: email.field.text ?? "", password: password.field.text ?? "")
    }
    
    func showMessage(_ message: String) {
        alert(message: message)
    }
}
