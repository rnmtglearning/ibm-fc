//
//  CountriesTableViewCell.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class CountriesTableViewCell: UITableViewCell {
    
    var flag = UIImageView()
    var name = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        flag.contentMode = .scaleToFill
        
        self.addSubview(flag)
        self.addSubview(name)
        
        self.addConstraint(pattern: "V:|[v0]|", views: name)
        self.addConstraint(pattern: "H:|-15-[v0(50)]-15-[v1]-5-|", views: flag, name)
        self.addConstraint(pattern: "V:[v0(30)]", views: flag)
        
        self.addConstraint(NSLayoutConstraint(item: flag, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
