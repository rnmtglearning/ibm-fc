//
//  CountriesTableTableViewController.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import UIKit

protocol CoutriesDelegate {
    func selected(id: Int, image: UIImage?)
}

class CountriesTableTableViewController: UITableViewController {
    
    var delegate: CoutriesDelegate?
    var dataSource = Countries.values

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(CountriesTableViewCell.self, forCellReuseIdentifier: "reuse")
        self.tableView.register(HeaderSearchView.self, forHeaderFooterViewReuseIdentifier: "header")
        self.tableView.allowsMultipleSelection = false
        self.tableView.allowsSelection = true
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? HeaderSearchView {
            headerView.field.field.addTarget(self, action: #selector(textFieldDidChange),
                                                 for: .editingChanged)
            return headerView
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath) as? CountriesTableViewCell {
            let value = dataSource[indexPath.row].1
            let key = dataSource[indexPath.row].0
            cell.name.text = value
            cell.flag.image = UIImage(named: "\(key)-\(value.replacingOccurrences(of: " ", with: "_"))")
            cell.selectionStyle = .default
            return cell
        }
        return tableView.dequeueReusableCell(withIdentifier: "reuse", for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alert(title: dataSource[indexPath.row].1, message: "Confirma a escolha desse time?", button: "Sim", buttonCancel: "Nao") { (action) in
            self.dismiss(animated: true, completion: {
                if let cell = tableView.cellForRow(at: indexPath) as? CountriesTableViewCell {
                    self.delegate?.selected(id: self.dataSource[indexPath.row].0, image: cell.flag.image)
                }
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
//        dataSource = Countries.values.filter({ (arg) -> Bool in
//            let (_, name) = arg
//            return name.starts(with: textField.text ?? "")
//        })
        
        if let text = textField.text {
            dataSource = Countries.values.filter {
                    $0.1.lowercased().starts(with: text.lowercased())
            }
        } else {
            dataSource = Countries.values
        }
        self.tableView.reloadData()
    }

}
