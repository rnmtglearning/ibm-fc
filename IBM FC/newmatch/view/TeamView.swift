//
//  TeamView.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class TeamView: UIView, UITextViewDelegate {
    
    var name: UILabel = {
        let l = UILabel()
        l.text = "Nome"
        l.textAlignment = .center
        l.backgroundColor = UIColor.whiteSmoke
        l.layer.radius(5)
        l.font = UIFont.systemFont(ofSize: 16)
        l.textColor = UIColor.black
        return l
    }()
    
    var flag: UIImageView = {
        let i = UIImageView()
        i.image = #imageLiteral(resourceName: "ic_add_large")
        i.contentMode = .scaleToFill
        i.backgroundColor = UIColor.whiteSmoke
        i.layer.radius(5)
        return i
    }()
    
    var side: UILabel = {
        let l = UILabel()
        l.text = "Mandante"
        l.backgroundColor = UIColor.whiteSmoke
        l.textAlignment = .center
        l.layer.radius(5)
        l.font = UIFont.systemFont(ofSize: 16)
        l.textColor = UIColor.black
        return l
    }()
    
    lazy var score: UITextView = {
        let t = UITextView()
        t.keyboardType = .numberPad
        t.layer.radius(5)
        t.font = UIFont.systemFont(ofSize: 40)
        t.textAlignment = .center
        t.text = "0"
        t.delegate = self
        t.returnKeyType = .done
        return t
    }()
    
    lazy var fields: UIStackView = {
        let s = UIStackView()
        s.axis = .vertical
        s.distribution = .equalCentering
        s.alignment = UIStackViewAlignment.center
        s.spacing = 10
        
        s.addArrangedSubview(name)
        s.addArrangedSubview(flag)
        s.addArrangedSubview(side)
        s.addArrangedSubview(score)
        
        s.addConstraints(pattern: "H:[v0(120)]", views: name, flag, side, score)
        s.addConstraints(pattern: "V:[v0(60)]", views: flag, score)
        s.addConstraints(pattern: "V:[v0(40)]", views: name, side)
        
        s.addConstraint(NSLayoutConstraint(item: name, attribute: .centerX, relatedBy: .equal, toItem: s, attribute: .centerX, multiplier: 1, constant: 0))
        s.addConstraint(NSLayoutConstraint(item: flag, attribute: .centerX, relatedBy: .equal, toItem: s, attribute: .centerX, multiplier: 1, constant: 0))
        s.addConstraint(NSLayoutConstraint(item: side, attribute: .centerX, relatedBy: .equal, toItem: s, attribute: .centerX, multiplier: 1, constant: 0))
        s.addConstraint(NSLayoutConstraint(item: score, attribute: .centerX, relatedBy: .equal, toItem: s, attribute: .centerX, multiplier: 1, constant: 0))
        return s
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(fields)
        self.addConstraint(pattern: "H:|[v0]|", views: fields)
        self.addConstraint(pattern: "V:|[v0]|", views: fields)
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        return newText.count < 3;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
