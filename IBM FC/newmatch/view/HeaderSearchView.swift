//
//  HeaderSearchTableViewCell.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class HeaderSearchView: UITableViewHeaderFooterView {
    
    let field: IBMTextField = {
        let t = IBMTextField()
        t.field.keyboardType = .namePhonePad
        t.field.autocapitalizationType = .words
        t.field.placeholder = "Pesquisar"
        return t
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.addSubview(field)
        
        self.addConstraint(pattern: "V:|[v0]|", views: field)
        self.addConstraint(pattern: "H:|[v0]|", views: field)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
