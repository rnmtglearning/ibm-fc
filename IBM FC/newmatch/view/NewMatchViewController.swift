//
//  NewMatchViewController.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import UIKit

class NewMatchViewController: UIViewController, NewMatchViewModule {
    
    var presenter: NewMatchPresenterModule?
    var homeSelected: Bool?
    var homeId: Int = 0
    var awayId: Int = 0
    
    lazy var home: TeamView = {
        let h = TeamView()
        h.name.text = "Pais 1"
        h.side.text = "MANDANTE"
        h.flag.isUserInteractionEnabled = true
        h.flag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openHomeCountries)))
        return h
    }()
    
    lazy var away: TeamView = {
        let a = TeamView()
        a.name.text = "Pais 2"
        a.side.text = "VISITANTE"
        a.flag.isUserInteractionEnabled = true
        a.flag.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openAwayCountries)))
        return a
    }()
    
    let vs: UILabel = {
        let l = UILabel()
        l.text = "VS"
        l.textColor = UIColor.whiteSmoke
        l.font = UIFont.systemFont(ofSize: 40)
        l.textAlignment = .center
        return l
    }()
    
    lazy var fields: UIStackView = {
        let s = UIStackView()
        s.axis = .horizontal
        s.distribution = .equalCentering
        s.alignment = UIStackViewAlignment.center
        s.spacing = 20
        
        s.addArrangedSubview(home)
        s.addArrangedSubview(vs)
        s.addArrangedSubview(away)
        
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.oxfordBlue
        
        setupView()
        setupNavigationController()
    }
    
    fileprivate func setupView() {
        self.view.addSubview(fields)
        self.view.addConstraint(pattern: "H:[v0]", views: fields)
        self.view.addConstraint(pattern: "V:|-100-[v0]", views: fields)
        self.view.addConstraint(NSLayoutConstraint(item: fields, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0))
    }
    
    fileprivate func setupNavigationController() {
        self.title = "Novo jogo"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveMatch))
    }
    
    @objc func saveMatch() {
        let homeScore = Int(home.score.text) ?? 0
        let awayScore = Int(away.score.text) ?? 0
        presenter?.save(homeId: homeId, awayId: awayId, homeScore: homeScore, awayScore: awayScore)
    }
    
    @objc func openHomeCountries() {
        openCountries(home: true)
    }
    
    @objc func openAwayCountries() {
        openCountries(home: false)
    }
    
    func openCountries(home: Bool) {
        let popover = CountriesTableTableViewController(style: UITableViewStyle.plain)
        popover.delegate = self
        popover.modalPresentationStyle = .overCurrentContext
        
        present(popover, animated: true, completion: nil)
        
        if (home) {
            homeSelected = true
        } else {
            homeSelected = false
        }
    }
    
    func selected(id: Int, image: UIImage?) {
        if let selected = homeSelected {
            if selected {
                home.name.text = (Countries.values.filter { $0.0 == id})[0].1
                home.flag.image = image
                homeId = id
            } else {
                away.name.text = (Countries.values.filter { $0.0 == id})[0].1
                away.flag.image = image
                awayId = id
            }
        }
    }
    
    func showMessage(message: String) {
        alert(message: message)
    }
}
