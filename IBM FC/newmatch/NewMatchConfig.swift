//
//  NewMatchConfig.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

protocol NewMatchRouterModule {
    
    func navigateNewMatch(from: UINavigationController)
    
    func close()

}

protocol NewMatchPresenterModule {
    
    var view: NewMatchViewModule? { get set }
    var router: NewMatchRouterModule? { get set }
    
    func save(homeId: Int, awayId: Int, homeScore: Int, awayScore: Int)
    
}

protocol NewMatchViewModule: CoutriesDelegate {
    
    var presenter: NewMatchPresenterModule? { get set }
    
    func showMessage(message: String)

}

class NewMatchConfig: Configurable {
    
    typealias T = NewMatchPresenterModule
    
    static let shared = NewMatchConfig()
    
    func configure() -> NewMatchPresenterModule {
        
        let view = NewMatchViewController()
        let presenter = NewMatchPresenter()
        
        view.presenter = presenter
        presenter.view = view
        
        return presenter
    }
    
}
