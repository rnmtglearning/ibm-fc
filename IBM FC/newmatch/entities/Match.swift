//
//  Match.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import RealmSwift

class Match: Object {
    
    @objc dynamic var gameId = 0
    @objc dynamic var teamHomeId = 0
    @objc dynamic var teamAwayId = 0
    @objc dynamic var scoreHomeId = 0
    @objc dynamic var scoreAwayId = 0

}
