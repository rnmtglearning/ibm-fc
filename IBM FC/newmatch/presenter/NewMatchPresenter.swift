//
//  NewMatchPresenter.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import RealmSwift

class NewMatchPresenter: NewMatchPresenterModule {
    
    var view: NewMatchViewModule?
    var router: NewMatchRouterModule?
    
    func save(homeId: Int, awayId: Int, homeScore: Int, awayScore: Int) {
        
        if homeId == 0 || awayId == 0 {
            view?.showMessage(message: "Time visitante ou mandante invalido.")
        } else {
        
            let match = Match()
            match.teamHomeId = homeId
            match.teamAwayId = awayId
            match.scoreHomeId = homeScore
            match.scoreAwayId = awayScore
            
            let realm = try! Realm()
            try! _ = realm.write {
                realm.add(match, update: false)
            }
        }
        
        router?.close()
        
    }
}
