//
//  NewMatchRouter.swift
//  IBM FC
//
//  Created by user140115 on 5/5/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

class NewMatchRouter: NewMatchRouterModule {
    
    fileprivate var nav: UINavigationController?
    
    let transition: CATransition = {
        let transition = CATransition()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        return transition
    }()
    
    func navigateNewMatch(from: UINavigationController) {
        var presenter = NewMatchConfig.shared.configure()
        presenter.router = self
        if let view = presenter.view as? UIViewController {
            from.view.layer.add(transition, forKey: nil)
            from.setNavigationBarHidden(false, animated: false)
            from.pushViewController(view, animated: true)
            nav = from
        }
    }
    
    func close() {
        nav?.popViewController(animated: true)
    }
    
}
