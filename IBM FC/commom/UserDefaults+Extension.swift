//
//  UserDefaults+Extension.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static let prefs = UserDefaults.standard
    
    enum key: String {
        case isLogged
    }
    
    static func bool(key: UserDefaults.key) -> Bool? {
        return UserDefaults.standard.bool(forKey: key.rawValue)
    }
    
    static func setBool(value: Bool, key: UserDefaults.key) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
    }
    
}
