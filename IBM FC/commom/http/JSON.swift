//
//  JSON.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

class Json<T: CodingKey> {
    
    let container: KeyedDecodingContainer<T>
    
    init(decoder: Decoder, keyedBy type: T.Type) {
        container = try! decoder.container(keyedBy: type)
    }
    
    func bool(forKey key: KeyedDecodingContainer<T>.Key) -> Bool? {
        do {
            return try container.decodeIfPresent(Bool.self, forKey: key)
        } catch {
        }
        return nil
    }
    
    func string(forKey key: KeyedDecodingContainer<T>.Key) -> String? {
        do {
            return try container.decodeIfPresent(String.self, forKey: key)
        } catch {
        }
        return nil
    }
    
    func int(forKey key: KeyedDecodingContainer<T>.Key) -> Int? {
        do {
            return try container.decodeIfPresent(Int.self, forKey: key)
        } catch {
        }
        return nil
    }
    
    func array<R>(forKey key: KeyedDecodingContainer<T>.Key) -> [R]? where R : Decodable {
        do {
            return try container.decodeIfPresent([R].self, forKey: key)
        } catch {
        }
        return nil
    }
    
}
