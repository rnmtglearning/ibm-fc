//
//  RequestAPI.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

public class RequestAPI {
    
    fileprivate let baseUrl: String = "https://private-95e2f2-ibmfc.apiary-mock.com"
    
    static let shared: RequestAPI = RequestAPI()
    
    func json(_ path: String, method: HTTPMethod = .get, parameters: [String : Any]? = nil, callback: @escaping (Data?, Int, Bool) -> Void) {
        if let url = URL(string: baseUrl + path) {
            let headers: HTTPHeaders = [ "Authorization": KeychainWrapper.loadString(key: .token) ?? ""]
            Alamofire.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
                
                if let data = response.data, let request = response.request {
                    print(request)
                    print("Data: \(String(describing: data))")
                }
                
                callback(response.data, response.response?.statusCode ?? 404, response.result.isSuccess)
            }
        }
    }
}
