//
//  UIColor+Extension.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    static var oxfordBlue = UIColor.fromHex(0x091D41)
    static var oxfordBlueLight = UIColor.fromHex(0xDCDEE2)
    static var prussianBlue = UIColor.fromHex(0x1A2F4D)
    static var prussianBlueLight = UIColor.fromHex(0xE6E8EC)
    static var royalBlue = UIColor.fromHex(0x447DE7)
    static var darkGray = UIColor.fromHex(0xACACAC)
    static var lightGray = UIColor.fromHex(0xF4F4F4)
    static var whiteSmoke = UIColor.fromHex(0xF4F4F4)
    
    static func fromHex(_ value: UInt32) -> UIColor {
        let r = CGFloat((value & 0xFF0000) >> 16) / 255.0
        let g = CGFloat((value & 0x00FF00) >> 8) / 255.0
        let b = CGFloat(value & 0x0000FF) / 255.0
        let a = CGFloat(1.0)
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
