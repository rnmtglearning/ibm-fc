//
//  Configurable.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation

protocol Configurable {
    
    associatedtype T
    
    func configure() -> T
    
}
