//
//  UILayer+Extension.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

extension CALayer {
    
    func radius(_ radius: CGFloat, color: UIColor = UIColor.clear, width: CGFloat = 1) {
        masksToBounds = true
        cornerRadius = radius
        borderWidth = width
        borderColor = color.cgColor
    }
    
}
