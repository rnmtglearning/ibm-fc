//
//  UIImageView+Extension.swift
//  IBM FC
//
//  Created by user140115 on 5/6/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

extension UIImageView {
    
    func dowloadImage(url: String) {
        if let url = URL(string: url) {
            Alamofire.request(url).response { (data) in
                if let data = data.data {
                    if let image = UIImage(data: data, scale:1) {
                        self.image = image
                    }
                }
            }
        }
    }
}
