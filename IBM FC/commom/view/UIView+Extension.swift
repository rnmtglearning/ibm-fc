//
//  UIView+Extension.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func addConstraint(pattern: String, views: UIView...) {
        var mViews: [String : UIView] = [:]
        
        for (index, view) in views.enumerated() {
            view.translatesAutoresizingMaskIntoConstraints = false
            mViews["v\(index)"] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: pattern,
                                                      options: NSLayoutFormatOptions(),
                                                      metrics: nil, views: mViews))
    }
    
    func fadeIn(_ duration: TimeInterval = 2) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1
        }) { _ in
        }
    }
    
    func addConstraints(pattern: String, views: UIView...) {
        for (_, view) in views.enumerated() {
            addConstraint(pattern: pattern, views: view)
        }
    }
    
}

extension UIViewController {
    
    func alert(title: String? = nil, message: String, button: String = "OK", buttonCancel: String? = nil, completion: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: button, style: UIAlertActionStyle.default, handler: completion))
        if let cancel = buttonCancel {
            alert.addAction(UIAlertAction(title: cancel, style: UIAlertActionStyle.destructive, handler: nil))
        }
        self.present(alert, animated: true, completion: nil)
    }
    
}
