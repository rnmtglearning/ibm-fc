//
//  Keychain+Extension.swift
//  IBM FC
//
//  Created by Ramon Cardoso on 04/05/18.
//  Copyright © 2018 Ramon Cardoso. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

extension KeychainWrapper {
        
    enum key: String {
        case token
    }
    
    static func loadString(key: KeychainWrapper.key) -> String? {
        return KeychainWrapper.standard.string(forKey: KeychainWrapper.key.token.rawValue)
    }
    
    static func setString(value: String, key: KeychainWrapper.key) -> Bool {
        return KeychainWrapper.standard.set(value, forKey: key.rawValue)
    }
    
    static func remove(key: KeychainWrapper.key) {
        KeychainWrapper.standard.removeObject(forKey: key.rawValue)
    }
    
}
